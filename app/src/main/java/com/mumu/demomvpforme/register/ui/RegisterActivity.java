package com.mumu.demomvpforme.register.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.mumu.demomvpforme.R;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_content, new RegisterFragment()).commit();

    }
}
