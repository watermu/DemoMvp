package com.mumu.demomvpforme.register.contract;

import com.mumu.demomvpforme.register.model.OnRegisterListener;

/**
 * Created by water_mu on 2017/7/23.
 */

public interface RegisterContract {
    interface Model {
        void requestRegister(String name, String password, OnRegisterListener listener);
    }

    interface View {
        String getName();

        String getPassword();

        void showSuccess(String text);

        void showFailed(String text);
    }

    interface Presenter {
        void register();

        void destroy();
    }
}
