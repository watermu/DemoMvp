package com.mumu.demomvpforme.register.model;

import android.os.Handler;
import android.os.Message;

import com.mumu.demomvpforme.register.contract.RegisterContract;

/**
 * Created by water_mu on 2017/7/23.
 */

public class RegisterModel implements RegisterContract.Model {

    private String name;
    private String password;
    private OnRegisterListener listener;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (listener != null) {
                if (!"world".equals(name)) {
                    listener.onFailed("用户名已存在,请重新填写");
                    return;
                }
                if (!"123".equals(password)) {
                    listener.onFailed("密码格式不正确");
                    return;
                }
                listener.onSuccess("注册成功");

            }
        }
    };
    @Override
    public void requestRegister(String name, String password, OnRegisterListener listener) {

        this.name = name;
        this.password = password;
        this.listener = listener;

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.sendEmptyMessage(2);

            }
        }).start();
    }
}
