package com.mumu.demomvpforme.register.ui;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mumu.demomvpforme.R;
import com.mumu.demomvpforme.login.ui.MainActivity;
import com.mumu.demomvpforme.register.contract.RegisterContract;
import com.mumu.demomvpforme.register.presenter.RegisterPresenter;

import static com.mumu.demomvpforme.R.id.et_name;
import static com.mumu.demomvpforme.R.id.et_passwarld;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment implements View.OnClickListener, RegisterContract.View {


    private EditText etName;
    private EditText etPasswarld;
    private Button btnRegister;
    private Button btnClear;
    private TextView tvResult;
    private RegisterContract.Presenter registerPresenter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        initView(view);
        registerPresenter = new RegisterPresenter(this);
        return view;
    }

    private void initView(View view) {
        etName = (EditText) view.findViewById(et_name);
        etPasswarld = (EditText) view.findViewById(et_passwarld);
        btnRegister = (Button) view.findViewById(R.id.btn_register);
        btnClear = (Button) view.findViewById(R.id.btn_clear);
        tvResult = (TextView) view.findViewById(R.id.tv_result);

        btnRegister.setOnClickListener(this);
        btnClear.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register:
                submit();
                break;
            case R.id.btn_clear:
                etName.setText("");
                etPasswarld.setText(null);
                break;
        }
    }

    private void submit() {

        String name = etName.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(getContext(), "请填写注册用户名", Toast.LENGTH_SHORT).show();
            return;
        }

        String passwarld = etPasswarld.getText().toString().trim();
        if (TextUtils.isEmpty(passwarld)) {
            Toast.makeText(getContext(), "请填写注册用户密码", Toast.LENGTH_SHORT).show();
            return;
        }
        registerPresenter.register();

    }

    @Override
    public String getName() {
        return etName.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return etPasswarld.getText().toString().trim();
    }

    @Override
    public void showSuccess(String text) {
        tvResult.setText(text);
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void showFailed(String text) {
        tvResult.setText(text);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroy() {
        registerPresenter.destroy();
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
