package com.mumu.demomvpforme.register.presenter;

import com.mumu.demomvpforme.register.contract.RegisterContract;
import com.mumu.demomvpforme.register.model.OnRegisterListener;
import com.mumu.demomvpforme.register.model.RegisterModel;

/**
 * Created by water_mu on 2017/7/23.
 */

public class RegisterPresenter implements RegisterContract.Presenter, OnRegisterListener {
    private RegisterContract.View view;
    private RegisterContract.Model registerModel;

    public RegisterPresenter(RegisterContract.View view) {

        this.view = view;
        registerModel = new RegisterModel();
    }

    @Override
    public void register() {
        registerModel.requestRegister(view.getName(), view.getPassword(), this);
    }

    @Override
    public void destroy() {
        view = null;
    }

    @Override
    public void onSuccess(String text) {
        view.showSuccess(text);
    }

    @Override
    public void onFailed(String text) {
        view.showFailed(text);
    }
}
