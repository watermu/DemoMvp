package com.mumu.demomvpforme.register.model;

/**
 * Created by water_mu on 2017/7/23.
 */

public interface OnRegisterListener {
    void onSuccess(String text);

    void onFailed(String text);
}
