package com.mumu.demomvpforme.login.presenter;

import com.mumu.demomvpforme.login.contract.LoginContract;
import com.mumu.demomvpforme.login.model.LoginModel;
import com.mumu.demomvpforme.login.model.OnLoginListener;

/**
 * Created by water_mu on 2017/7/23.
 */

public class LoginPresenter implements LoginContract.Presenter, OnLoginListener {
    LoginContract.View view;
    LoginContract.Model loginModel;

    public LoginPresenter() {
        loginModel = new LoginModel();
    }

    public LoginPresenter(LoginContract.View view) {
        this.view = view;
        loginModel = new LoginModel();
    }

    @Override
    public void login() {
        loginModel.requestLogin(view.getName(), view.getPassword(), this);
    }

    @Override
    public void attach(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        view = null;
    }

    @Override
    public void onSuccess(String success) {
        view.showSuccess(success);
    }

    @Override
    public void onFailed(String failed) {
        view.showFailed(failed);
    }
}
