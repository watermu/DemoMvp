package com.mumu.demomvpforme.login.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mumu.demomvpforme.R;
import com.mumu.demomvpforme.login.contract.LoginContract;
import com.mumu.demomvpforme.login.presenter.LoginPresenter;

public class MainActivity extends AppCompatActivity implements LoginContract.View, View.OnClickListener {

    private EditText etName;
    private EditText etPasswarld;
    private Button btnLogin;
    private TextView tvResultSuccess;
    private TextView tvResultFailed;
    private LoginContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        presenter = new LoginPresenter();
        presenter.attach(this);
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        presenter=null;
        super.onDestroy();
    }

    @Override
    public String getName() {
        return etName.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return etPasswarld.getText().toString().trim();
    }

    @Override
    public void showSuccess(String content) {
        tvResultSuccess.setText(content);
    }

    @Override
    public void showFailed(String content) {
        tvResultFailed.setText(content);
    }

    private void initView() {
        etName = (EditText) findViewById(R.id.et_name);
        etPasswarld = (EditText) findViewById(R.id.et_passwarld);
        btnLogin = (Button) findViewById(R.id.btn_login);
        tvResultSuccess = (TextView) findViewById(R.id.tv_result_success);
        tvResultFailed = (TextView) findViewById(R.id.tv_result_failed);

        btnLogin.setOnClickListener(this);

//        presenter = new LoginPresenter(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                submit();
                break;
        }
    }

    private void submit() {
        // validate
        String name = etName.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "请输入姓名", Toast.LENGTH_SHORT).show();
            return;
        }

        String passwarld = etPasswarld.getText().toString().trim();
        if (TextUtils.isEmpty(passwarld)) {
            Toast.makeText(this, "请输入密码", Toast.LENGTH_SHORT).show();
            return;
        }
        presenter.login();
    }
}
