package com.mumu.demomvpforme.login.model;

import android.os.Handler;
import android.os.Message;

import com.mumu.demomvpforme.login.contract.LoginContract;

/**
 * Created by water_mu on 2017/7/23.
 */

public class LoginModel implements LoginContract.Model {
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (listener != null) {
                if (!"hello".equals(name)) {
                    listener.onFailed("用户名不正确");
                    return;
                }
                if (!"123".equals(password)) {
                    listener.onFailed("密码错误");
                    return;
                }
                listener.onSuccess("登陆成功");

            }
        }
    };
    private String name;
    private String password;
    private OnLoginListener listener;

    @Override
    public void requestLogin(String name, String password, OnLoginListener listener) {
        this.name = name;
        this.password = password;
        this.listener = listener;

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.sendEmptyMessage(1);

            }
        }).start();
    }
}
