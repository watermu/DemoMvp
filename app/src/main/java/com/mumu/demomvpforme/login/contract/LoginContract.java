package com.mumu.demomvpforme.login.contract;

import com.mumu.demomvpforme.login.model.OnLoginListener;

/**
 * Created by water_mu on 2017/7/23.
 */

public interface LoginContract {
    interface Model {
        void requestLogin(String name, String password, OnLoginListener listener);
    }

    interface View {
        String getName();
        String getPassword();

        void showSuccess(String content);

        void showFailed(String content);
    }

    interface Presenter {
        /**
         * 绑定View
         */
        void attach(View view);

        /**
         * 与View分离
         */
        void detach();

        void login();

    }
}
