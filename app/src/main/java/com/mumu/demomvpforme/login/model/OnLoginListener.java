package com.mumu.demomvpforme.login.model;

/**
 * Created by water_mu on 2017/7/23.
 */

public interface OnLoginListener {
    void onSuccess(String success);
    void onFailed(String failed);
}
